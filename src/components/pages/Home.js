import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

function Home() {
	const [users, setUsers] = useState([]);

	useEffect(() => {
		retriveUsers();
	}, []);

	const retriveUsers = async () => {
		const result = await axios.get("http://localhost:3005/users");
		setUsers(result.data.reverse());
	};

	const deleteUser = async (id) => {
		await axios.delete(`http://localhost:3005/users/${id}`);
		retriveUsers();
	};

	return (
		<div className="container">
			<div className="py-4">
				<h1>Home Page</h1>
			</div>
			<table className="table border shadow">
				<thead className="table-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">UserName</th>
						<th scope="col">Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{users.map((user, index) => (
						<tr>
							<th scope="row	">{index + 1}</th>
							<td>{user.name}</td>
							<td>{user.username}</td>
							<td>{user.email}</td>
							<td>
								<Link
									className="btn btn-primary me-2"
									to={`/viewUser/${user.id}`}
								>
									View
								</Link>
								<Link
									className="btn btn-outline-primary me-2"
									to={`/editUser/${user.id}`}
								>
									Edit
								</Link>
								<Link
									className="btn btn-danger me-2"
									onClick={() => deleteUser(user.id)}
								>
									Delete
								</Link>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default Home;
