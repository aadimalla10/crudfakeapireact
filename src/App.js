import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./components/pages/Home";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import Navbar from "./components/Layout/Navbar";
import AddUser from "./components/pages/AddUser";
import View from "./components/pages/View";
import Edit from "./components/pages/Edit";

function App() {
	return (
		<div className="App">
			<Router>
				<Navbar />
				<Switch>
					<Route path="/" exact={true} component={Home}></Route>
					<Route path="/contacts" component={Contact}></Route>
					<Route path="/about" component={About}></Route>
					<Route path="/addUser" component={AddUser}></Route>
					<Route path="/viewUser/:id" component={View}></Route>
					<Route path="/editUser/:id" component={Edit}></Route>
				</Switch>
			</Router>
		</div>
	);
}

export default App;
